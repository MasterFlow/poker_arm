%function for the dobot class
%test line

L1 = Link ('d', 0, 'a', 80, 'alpha', pi/2);
L2 = Link ('d', 3 * pi/4, 'a', 0, 'alpha', 0);
L3 = Link ('d', 8 * pi/9, 'a', 0, 'alpha', 0);

robot = SerialLink ([L1 L2 L3], 'name', 'bot'); 